const Discord = require("discord.js");
const auth = require("./auth.json");
const botconfig = require("./botconfig.json");

const bot = new Discord.Client({disableEveryone: true});

bot.on("ready", async () => {
    console.log(`${bot.user.username} is online!`);
    bot.user.setActivity("your messages", {type: "WATCHING"});
});

bot.on("message", async message => {
    if(message.author.bot) return;
    if(message.channel.type === "dm") return;

    let prefix = botconfig.prefix;
    let messageArray = message.content.split(" ");
    let cmd = messageArray[0];
    let args = messageArray.slice(1);


    //Ban command
    if(cmd === `${prefix}ban`){

        let bUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
        if(!bUser) return message.channel.send("Couldn't find user.");
        let bReason = args.join(" ").slice(22);
        if(!message.member.hasPermission("MANAGE_MEMBERS")) return message.channel.send("You don't have permission to do this.");
        if(!bUser.hasPermission("MANAGE_MESSAGES")) return message.channel.send("That person can't be banned.");

        let bannedEmbed = new Discord.RichEmbed()
        .setDescription("~Banned~")
        .setColor("#E0DDB2")
        .addField("User Banned", `${bUser} with ID: ${bUser.id}`)
        .addField("Banned By", `<@${message.author.id}>  with ID: ${message.author.id}}`)
        .addField("Banned In", message.channel)
        .addField("Time", message.createdAt)
        .addField("Reason", bReason);

        let incidentChannel = message.guild.channels.find(x => x.name === "incidents");
        if(!incidentChannel) return message.channel.send("Couldn't find incidents channel.");

        message.guild.member(bUser).ban(bReason);
        incidentChannel.send(banEmbed);

        return;
    }

    //Kick command
    if(cmd === `${prefix}kick`){

        let kUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
        if(!kUser) return message.channel.send("Couldn't find user.");
        let kReason = args.join(" ").slice(22);
        if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.channel.send("You don't have permission to do this.");
        if(!kUser.hasPermission("MANAGE_MESSAGES")) return message.channel.send("That person can't be kicked.");

        let kickEmbed = new Discord.RichEmbed()
        .setDescription("~Kicked~")
        .setColor("#E0DDB2")
        .addField("User Kicked", `${kUser} with ID: ${kUser.id}`)
        .addField("Kicked By", `<@${message.author.id}>  with ID: ${message.author.id}}`)
        .addField("Kicked In", message.channel)
        .addField("Time", message.createdAt)
        .addField("Reason", kReason);

        let incidentChannel = message.guild.channels.find(x => x.name === "incidents");
        if(!incidentChannel) return message.channel.send("Couldn't find incidents channel.");

        message.guild.member(kUser).kick(kReason);
        incidentChannel.send(kickEmbed);

        return;
    }

    //Report command
    if(cmd === `${prefix}report`){

        let rUser = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
        if(!rUser) return message.channel.send("Couldn't find user.");
        let reason = args.join(" ").slice(22);

        let reportEmbed = new Discord.RichEmbed()
        .setDescription("Reports")
        .setColor("#E0DDB2")
        .addField("Reported User", `${rUser} with ID: ${rUser.id}`)
        .addField("Reported By", `${message.author} with ID: ${message.author.id}`)
        .addField("Channel", message.channel)
        .addField("Time", message.createdAt)
        .addField("Reason", reason);

        let reportschannel = message.guild.channels.find(x => x.name === "reports");
        if(!reportschannel) return message.channel.send("Couldn't find reports channel.");

        message.delete().catch(O_o=>{});
        reportschannel.send(reportEmbed);
        
        return;
    }

    //Help command
    if(cmd === `${prefix}help`){

        let bicon = bot.user.displayAvatarURL;
        let helpembed = new Discord.RichEmbed()
        .setColor("#E0DDB2")
        .setThumbnail(bicon)
        .addField("Discord-Bot Commands", "Here are some basic commands.")
        .addField("Bot Information | !binfo", "This will display information about Discord-Bot")
        .addField("Server Information | !sinfo", "This will display information about this server")
        .addField("Report a User | !report @username reason", "This command will report a user.");

        return message.channel.send(helpembed);
    }

    //Server Information command
    if(cmd === `${prefix}sinfo`){

        let sicon = message.guild.iconURL;
        let sembed = new Discord.RichEmbed()
        .setDescription("Server Information")
        .setColor("#E0DDB2")
        .setThumbnail(sicon)
        .addField("Server Name", message.guild.name)
        .addField("Created On", message.guild.createdAt)
        .addField("You Joined", message.member.joinedAt)
        .addField("Total Members", message.guild.memberCount);

        return message.channel.send(sembed);
    }

    //Bot Information command
    if(cmd === `${prefix}binfo`){

        let bicon = bot.user.displayAvatarURL;
        let botembed = new Discord.RichEmbed()
        .setDescription("Bot Information")
        .setColor("#E0DDB2")
        .setThumbnail(bicon)
        .addField("Bot Name", bot.user.username)
        .addField("Created On", bot.user.createdAt);

        return message.channel.send(botembed);
    }
});

bot.login(auth.token);
