# discord-bot

This repository contains the basic files needed for a locally deployed Discord Bot.

## Prerequisites

You will need a Discord Account and a Discord Server.  
[Click here to get started](https://discordapp.com/).

## Clone this repository

1. Clone this Repository
2. Install dependencies

```bash
npm install
```

## Create auth.json file

You will need to create an `auth.json` file and add the following:

```json
{
    "token": "<your_token>"
}
```


### Create your Discord Application

1. Navigate to [discordapp.com/developers/](https://discordapp.com/developers/) and log in with your Discord information if prompted
2. From the `Applications` tab, click the `New Application` button
3. Enter a `Name` and click the `Create` button

### Add the Bot to your Discord Server

1. Enter the following URL into your browser in another tab:

```
https://discordapp.com/oauth2/authorize?client_id=<client_id>&scope=bot&permissions=8
```

2. Navigate back to your [application](https://discordapp.com/developers/) and select it
3. Look for the `Client ID` section
4. Click on the `Copy` button
5. Paste your `Client ID` over `<client_id>` in the URL and submit it
6. Select your Discord Server from the drop down and click `Authorize`

### Getting your Token

1. Navigate back to your [application](https://discordapp.com/developers/) and select it
2. Click on your `Application`
3. Click on `Bot` on the left side panel
4. Within the `Build-A-Bot` section there is a `Token` section
5. Click on the `Copy` button
6. Paste your token over `<your_token>` and save the file

### Start the Bot

```bash
node bot.js
```

## Commands

Type `!help` to see the supported commands.